#!/bin/bash

export LONG_KERNEL_NAME=2.6.32-358.14.1.el6.$basearch
export SHORT_KERNEL_NAME=2.6.32-358.el6.$basearch
export INSTALL_KEY=38699-60149-36808-21840-05491
export ARCH=x86_64
export FIRST_PASS_OVER_FILE=/root/first_pass_over.txt
export first_pass_over=0

function install_wget() {
	###############
	# Basics

	# Installing basic utilities
	echo "Installing basic utilities..."
	yum install -y wget
	echo "Basic utilities installed."
}

function install_basics() {
	###############
	# SETUP

	#Installing EPEL repositories
	echo "Installing EPEL..."
	wget http://fedora.mirror.nexicom.net/epel/6/$ARCH/epel-release-6-8.noarch.rpm
	rpm -i epel-release-6-8.noarch.rpm
	echo "EPEL installed."

	echo "updating and installing new software from repository..."
	yum clean all
	yum -y update

	yum -y install unzip yum-utils rpmdevtools rpmlint redhat-rpm-config patchutils vim openssh-server xorg-x11-fonts-Type1 xorg-x11-fonts-misc firefox gcc gcc-c++ make kernel-devel ncurses-devel kernel-devel kernel-headers lshw
    echo Installed required software.
}

function set_eth0_up_at_boot() {
	# Changing default network configuration
	sed -i 's/ONBOOT=no/ONBOOT=yes/g' /etc/sysconfig/network-scripts/ifcfg-eth0
}

function install_graphical_ui() {
	echo "Installing GUI..."
    yum -y install glibc.i686 libgcc.i686 compat-libstdc++.i686 expat.i686 glibc.i686 glibc-devel.i686 libdrm.i686 libgcc.i686 libselinux.i686 libstdc++.i686 libX11.i686 libXau.i686 libxcb.i686 libXdamage.i686 libXext.i686 libXinerama.i686 libXfixes.i686 libXxf86vm.i686 mesa-dri-drivers.i686 mesa-libGL.i686 nss-softokn-freebl.i686 zlib.i686

	yum -y groupinstall Xfce
	# Graphical interface
	echo id:5:initdefault: > /etc/inittab

	yum -y install dejavu-lgc-sans-mono-fonts
	echo "Finished setting up GUI."
}

function add_realtime_kernel_repositories() {
	###############
	# New Kernel

	echo "Installing new Realtime Kernel..."
	wget http://glitesoft.cern.ch/cern/slc44/x86_64/RPM-GPG-KEYs/RPM-GPG-KEY-cern
	mv RPM-GPG-KEY-cern /etc/pki/rpm-gpg/

	# Create repository for MRG Realtime kernel
	cat << 'EOF' > /etc/yum.repos.d/mrg-realtime.repo
[slc6-mrg]
name=Scientific Linux CERN (SLC6) - MRG addons
baseurl=http://linuxsoft.cern.ch/cern/mrg/slc6X/$basearch/yum/mrg/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=1
protect=1

[slc6-mrg-source]
name=Scientific Linux CERN (SLC6) - MRG addons - source
baseurl=http://linuxsoft.cern.ch/cern/mrg/slc6X/$basearch/yum/mrg-source/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=0
protect=1

[slc6-mrg-debug]
name=Scientific Linux CERN (SLC6) - MRG addons - debug
baseurl=http://linuxsoft.cern.ch/cern/mrg/slc6X/$basearch/yum/mrg-debug/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=0
protect=1

[slc6-mrg-testing]
name=Scientific Linux CERN (SLC6) - MRG addons testing
baseurl=http://linuxsoft.cern.ch/cern/mrg/slc6X/testing/$basearch/yum/mrg-testing/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=0
protect=1

[slc6-mrg-testing-source]
name=Scientific Linux CERN (SLC6) - MRG addons - testing source
baseurl=http://linuxsoft.cern.ch/cern/mrg/slc6X/testing/$basearch/yum/mrg-testing-source/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=0
protect=1

[slc6-mrg-testing-debug]
name=Scientific Linux CERN (SLC6) - MRG addons - testing debug
baseurl=http://linuxsoft.cern.ch/cern/mrg/slc6X/testing/$basearch/yum/mrg-testing-debug/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=0
protect=1
EOF
}

function install_realtime_kernel() {
	# Remove newer 3.6 kernel and reinstall 2.6.33 (For NIDAQ cards and drivers)
	yum clean all
	yum -y groupinstall "MRG Realtime"
	yum -y remove kernel-rt
	yum -y install kernel-rt-2.6.33.9-rt31.75.el6rt.x86_64 kernel-rt-devel-2.6.33.9-rt31.75.el6rt.x86_64
	echo "Installed Realtime Kernel."
}

function setup_users() {
	###############
	# USERS

	echo "Adding acquisition user with password edcygv45"
	useradd -m -d /home/acquisition acquisition
	echo "acquisition:edcygv45" | /usr/sbin/chpasswd
	mkdir -p /home/acquisition/Desktop
	echo "User added."

	echo "Adding Desktop to ROOT Directory..."
	mkdir -p /root/Desktop
}

function end_first_pass() {
	touch $FIRST_PASS_OVER_FILE
}

function prepare_for_nidaq() {
	###############
	# NIDAQ preparation

	# Create symlink to kernel headers
	ln -s /usr/src/kernels/$LONG_KERNEL_NAME /usr/src/kernels/$SHORT_KERNEL_NAME
}

function mount_nidaq() {
	# Prepare mount points
	echo Mounting virtual driver disks...
	mkdir -p /mnt/NIDAQmxBase/
	mkdir -p /mnt/NIDAQmx/
	mount -t iso9660 -o loop nidaqmxbase-3.6.0.iso /mnt/NIDAQmxBase/
	mount -t iso9660 -o loop DAQmx802f0.iso /mnt/NIDAQmx/
	echo Driver disks mounted.
}

function install_nidaq() {
	# Launch installers
	echo "Installing NIDAQmxBase..."
	pushd .
	cd /mnt/NIDAQmxBase/
	./INSTALL
	popd
	echo "Finished."

	echo "Installing NIDAQmx..."
	pushd .
	cd /mnt/NIDAQmx/
	./INSTALL
	popd
	echo "Finished."
}

function install_python_environment() {
	#install pip
	echo "Installing python environment..."
	yum -y install python
	yum -y install scipy
	yum -y install numpy
	yum -y python-pip
	pip install PyDAQmx
	pip install pyserial
	echo "Finished installing python."
}

function install_python_acquisition() {
	echo "Installing acquisition software..."
	if [ -d "/home/root/DAQ-Python" ]
	then
		echo "Copying acquisition software to acquisition user's home"
		cp -r /root/DAQ-Python /home/acquisition
	else
		echo "WARNING: Acquisition software not in root'home directory."
	fi
	
}

function finalize() {
	###############
	# FINALIZE
	echo "Please start acquisition software from the link provided on the desktop interface."
}

function check_pass() {
	if [ -a $FIRST_PASS_OVER_FILE ]
	then 
            first_pass_over=1
	else
	    first_pass_over=0	
	fi
}

function first_pass() {
	install_wget
	install_basics
	set_eth0_up_at_boot
	install_graphical_ui
	add_realtime_kernel_repositories
	install_realtime_kernel
	setup_users
	end_first_pass
}

function second_pass() {
	prepare_for_nidaq
	mount_nidaq
	install_nidaq
	install_python_environment
	install_python_acquisition
	finalize
}

function main() {
	check_pass
	if [ $first_pass_over -eq 0 ] 
    then
		first_pass
	else 
		second_pass
	fi
}

main

