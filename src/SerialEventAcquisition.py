__author__ = 'lemelino'

import time
import logging

import serial

import commons
import config

logger = logging.getLogger("SerialEventAcquisition")
exitCode = False


def initSerialPort():
    """We configure the serial event port for acquisition."""
    interface = serial.Serial(port=config.SERIAL_INTERFACE, baudrate=config.BAUD_RATE, timeout=1)
    return interface


def onExit():
    """This function is called upon finishing an acquisition session."""
    logger.info("Closing Serial Event Acquisition...")
    logger.info("Serial Event Acquisition closed.")


def acquireEventCode(interface):
    """Reads an event code.  If nothing is on the line, returns None."""
    code = interface.read()

    try:
        code = int(code)
        logger.debug("Received event code '%s'!" % code)
    except:
        code = None

    return code


def pushEventCode(startTime, eventQueue, eventCode):
    """Pushes the event code accompanied by its event description and its time (relative to the beginning
    of the acquisition) to the data queue."""
    eventLatency = time.time() - startTime
    eventString = config.EVENTS[eventCode]

    logger.debug("Pushing the following data:")
    logger.debug("\t%s" % eventLatency)
    logger.debug("\t%s" % eventString)
    logger.debug("\t%s" % eventCode)

    eventQueue.put([eventLatency, eventString, eventCode])


def execute(eventQueue, controlPipe, timingQueue):
    """Main function, called when the process starts."""
    global exitCode

    logger.info("Starting Serial Event Acquisition Thread.")
    serialPort = initSerialPort()

    # Busy waiting
    commons.spinUntilStart(controlPipe, logger)

    # Timing queue: waiting that the acquisition process finishes to obtain its starting time.
    # Thus, the event acquisition is synchronized with the EEG acquisition.
    while timingQueue.empty():
        pass
    startTime = timingQueue.get()
    print startTime

    # Main loop, where we acquire events until the acquisition is stopped.
    while exitCode is False:
        eventCode = acquireEventCode(serialPort)
        if eventCode is not None:
            pushEventCode(startTime, eventQueue, eventCode)

        if controlPipe.poll():
            data = controlPipe.recv()
            if data == "exit":
                exitCode = True

    onExit()
