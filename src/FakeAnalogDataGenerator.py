__author__ = 'lemelino'

import time
import logging
import numpy
import commons
import config

logger = logging.getLogger("DataGenerator")
exitCode = False


def onExit():
    logger.info("Closing Analog Data Generator...")
    logger.info("Analog Data Generator closed.")


def createDataMatrix(controlPipe):
    dataMatrix = None
    try:
        dataMatrix = numpy.ndarray((config.NUMBER_OF_CHANNELS, config.SAMPLES_PER_SECOND))
        logger.info("Successfully created data acquisition matrix.")
    except:
        logger.critical("Could not allocate data acquisition matrix.")
        controlPipe.send(exit)

    return dataMatrix


def createRandomData(dataMatrix):
    logger.debug("Beginning random data generation...")

    t0 = time.time()
    for x in range(config.NUMBER_OF_CHANNELS):
        for y in range(config.SAMPLES_PER_SECOND):
            dataMatrix[x, y] = config.MAX_VOLTAGE / config.SAMPLES_PER_SECOND * y

    logger.debug("Finished random data generation. Took %s seconds." % (time.time() - t0))
    return dataMatrix


def execute(dataQueue, controlPipe, timingPipe, visualDataQueue):
    global exitCode

    logger.info("Starting Analog Data Generator Thread.")
    dataMatrix = createDataMatrix(controlPipe)

    commons.spinUntilStart(controlPipe, logger)

    timing = time.time()
    logger.info("starting data acquisition at %.6f." % timing)
    timingPipe.put(timing)

    while exitCode is False:
        newData = createRandomData(dataMatrix)

        try:
            dataQueue.put(newData)
            visualDataQueue.put(newData)
            logger.debug("Successfully pushed data.")
        except Exception:
            logger.critical("Could not push current data for queueing. Queue probably full.")
            controlPipe.send(exit)

        time.sleep(1)

        if controlPipe.poll():
            data = controlPipe.recv()
            if data == "exit":
                exitCode = True

    onExit()
