__author__ = 'lemelino'

import os.path
import logging
import commons
import time

exitCode = False
logger = logging.getLogger("EventWriter")


def checkData(data):
    """Makes sure the data received is sane."""
    try:
        if len(data) != 3:
            return False
    except Exception:
        return False
    return True


def prepareFile(controlPipe, fileName):
    """Prepares the file used to save the various events, and returns an handle."""

    if os.path.isfile(fileName):
        logger.critical("File %s already exists! Exiting." % fileName)
        controlPipe.send("exit")

    f = open(fileName, 'w')

    # Add Header information
    f.write("Latency\tType\tPosition\n")
    return f


def writeData(f, data):
    """Writes the data tuple in the event log."""
    try:
        f.write("%s\t%s\t%s\n" % (data[0], data[1], data[2]))
        return True
    except:
        logger.error("Could not write data.")
        return False


def onExit(f):
    """This function is called when we stop the acquisition."""
    logger.info("Closing EventWriter...")
    f.close()
    logger.info("Event Writer closed.")


def execute(eventQueue, controlPipe, fileName):
    """Execute the main program."""

    global exitCode

    logger.info("Starting Event Writer Thread.")

    if controlPipe is None:
        logger.error("Control Pipe is absent. Stopping.")
        controlPipe.send("exit")

    if eventQueue is None:
        logger.error("Event Queue is absent. Stopping.")
        eventQueue.send("exit")

    f = prepareFile(controlPipe, fileName)
    commons.spinUntilStart(controlPipe, logger)

    while exitCode is False:
        #Process event queue
        while not eventQueue.empty():
            data = eventQueue.get()

            # Validates the event.
            valid = checkData(data)
            if not valid:
                logger.error("Data '%s' is invalid. Should be number, string, number." % data)
                controlPipe.send("exit")

            # Validates that writing the data was successful.
            success = writeData(f, data)
            if not success:
                logger.error("Data '%s' is invalid. Should be number, string, number." % data)
                controlPipe.send("exit")

        #Process control pipe messages.
        if controlPipe.poll():
            data = controlPipe.recv()
            if data == "exit":
                exitCode = True

    onExit(f)
