__author__ = 'lemelino'

import time
import logging
import sys
import socket

import commons
import config

# Initializes global variables
logger = logging.getLogger("EthernetEventAcquisition")
exitCode = False

def readlines(sock, recv_buffer=40, delim='\n'):
    """Function used to read a single line from a socket."""
    buffer = ''
    data = True
    while data:
        data = sock.recv(recv_buffer)
        buffer += data

        while buffer.find(delim) != -1:
            line, buffer = buffer.split('\n', 1)
            yield line
    return


def waitForStart(conn):
    '''Busy-wait function used to wait for the start signal. The start signal is
    then sent to all other processes.'''
    # Waiting for start signal...
    signal = ""
    while "start" not in signal:
        signal = readlines(conn)


def waitForConnectAndStart(s, controlPipe):
    '''This method is used to wait for a connection on the configured port, then for
    the start signal. The function will periodically timeout to see if it must be closed
    by other sources.'''
    while True:
        try:
            # Accept connection from external program
            conn,addr = s.accept()
            s.settimeout(None)
            logger.info("accepted connection from %s." % addr[0])
            logger.info("Waiting for start signal...")
            
            # Waits for the start signal
            waitForStart(conn)

            # Received signal. Starting!
            logger.info("Received 'start' signal.  Starting!")
            controlPipe.send("start");

            # Return connection for event codes processing.
            return conn
        except socket.timeout:
            # In case another process specifies that it wants to quit, the timeout
            # permits frequent checks of the control pipe.
            if controlPipe.poll():
                data = controlPipe.recv()
                if data == "exit":
                    exitCode = True
                    return None


def initSocket(controlPipe):
    '''We initialize the socket for acquisition, waiting for a connection and for the start signal.'''
    # server socket methods
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        s.bind(('', config.ETHERNET_PORT))
        s.listen(1)
    except Exception:
        logger.error("Could not create server socket.")

    logger.info("Listening on port %s..." % config.ETHERNET_PORT)
   
    # Code to make sure we can exit, even though we wait for a connection.
    s.settimeout(2)
    return waitForConnectAndStart(s, controlPipe)


def onExit(connection):
    """This function is called upon finishing an acquisition session.
    it is used to clean up the process before exiting."""

    logger.info("Closing Ethernet Event Acquisition...")
    if connection is not None:
        try:
            socket.setblocking(False)
            data = connection.recv(1000)
        except Exception:
            pass
        connection.close()
       
    logger.info("Ethernet Event Acquisition closed.")


def acquireEventCode(connection, controlPipe):
    """Reads an event code.  If nothing is on the line, returns None."""
    global exitCode

    for code in readlines(connection):
        # MATLAB sends newlines.  We'll have to remove them.
        code = code.strip()
        connection.sendall("0\n")
        
        # If there is no code, return null reference 
        if not code or code == '':
            return None
    
        # If the finish code is on the line, send the exit signal.
        if code == "finish":
            logger.info("Received finish code...")
            controlPipe.send("exit")
            exitCode = True
            return None
    
        # Try to convert the numerical code to an integer.
        try:
            numCode = int(code)
            logger.debug("Received event code '%s'!" % code)
        except:
            logger.info("Received a BAD CODE: '%s'" % code)
            numCode = None
        return numCode


def pushEventCode(startTime, eventQueue, eventCode, visualEventQueue):
    """Pushes the event code accompanied by its event description and its time (relative to the beginning
    of the acquisition) to the data queue."""
    
    # Calculate time according to the starting time.
    eventLatency = time.time() - startTime

    # Lookup the event in the event table.
    try:
        eventString = config.EVENTS[eventCode]
    except Exception:
        logger.error("ERROR: Event %s does not exist in the event table!" % eventCode)

    # Debug string to indicate what is being pushed to the event writer.
    logger.info("Received event '%s' at time '%s'." % (eventString, eventLatency))
    logger.debug("Pushing the following data:")
    logger.debug("\t%s" % eventLatency)
    logger.debug("\t%s" % eventString)
    logger.debug("\t%s" % eventCode)

    # Put the event in the queue for processing by the event writer.
    eventQueue.put([eventLatency, eventString, eventCode])
    visualEventQueue.put([eventLatency, eventString, eventCode])


def execute(eventQueue, controlPipe, timingQueue, visualEventQueue):
    """Main function, called when the process starts."""
    global exitCode

    # Socket init.  If there is an error initializing, we quit.
    logger.info("Starting Ethernet Event Acquisition Thread.")
    connection = initSocket(controlPipe)
    if connection is None:
        exitCode = True
        onExit(connection)
        sys.exit(0)

    # Timing queue: waiting that the acquisition process finishes to obtain its starting time.
    # Thus, the event acquisition is synchronized with the EEG acquisition.
    # We wait for the start from the timing queue...
    while timingQueue.empty():
        pass
    startTime = timingQueue.get()
    logger.info("Starting at '%.6f'" % startTime)

    # While we want to run...
    while exitCode is False:
        # Acquire an event code
        eventCode = acquireEventCode(connection, controlPipe)
        # If valid, we push it to the event writer
        if eventCode is not None:
            pushEventCode(startTime, eventQueue, eventCode, visualEventQueue)

        # Handle usual signals if required
        if controlPipe.poll():
            data = controlPipe.recv()
            if data == "exit":
                exitCode = True

    # Exit
    onExit(connection)
