__author__ = 'lemelino'

import logging
import time
import threading
import signal


class TerminalController(threading.Thread):
    """This module is a thread running in the main process. It is used to control the acquisition, and start and stop
    various subsystems."""

    def onExit(self):
        """Called upon stopping the acquisition."""
        self.logger.info("Closing Controller...")
        self.logger.info("Controller closed.")

    def handleCommand(self, command):
        """Function used to handle the various commands written by the user. Returns True if we need to stop
        the acquisition."""
        try:
            if command == "exit" or command == "stop":
                self.controlPipe.send("exit")
                return True
            elif command == "help":
                print("Type 'exit' or 'stop' to end current acquisition session, or connect another application through TCP/IP with the given port to begin recording.")
            elif command == "":
                pass
            else:
                print("Could not understand this command: %s" % command)
        except Exception:
            self.logger.info("Could not send command.  Exiting.")
            self.exitCode = True

        return False

    def init(self, controlPipe):
        """Initializes the controller."""

        self.exitCode = False

        self.logger = logging.getLogger("Controller")
        self.logger.info("Starting Controller Thread.")

        if controlPipe is None:
            self.logger.critical("Control Pipe is absent. Stopping.")
        else:
            self.controlPipe = controlPipe


    def run(self):
        """Main function. This is called when the thread is told to run."""
        print("Welcome to the acquisition system.\n  To exit the recording before it is over, enter 'exit'. For help with the immediate use of the system, enter 'help'.")

        while self.exitCode is False:
            command = raw_input("shell>")
            
            if command != '':
                self.exitCode = self.handleCommand(command)

            if self.controlPipe.poll():
                data = self.controlPipe.recv()
                if data == "exit":
                    self.exitCode = True

        self.onExit()
