__author__ = 'lemelino'

import os
import sys
import time
import signal
import logging
from multiprocessing import Process, Queue, Pipe

import config
import EventWriter
import AnalogDataWriter
import FakeAnalogDataGenerator
import EthernetEventAcquisition
import Controller
import Grapher

globalExitCode = False

def generateFilename():
    """Generates a filename for a recording session."""
    return time.strftime("recording_%Y_%m_%d-%H_%M_%S")


def verifyWeAreRoot():
    return os.geteuid() == 0


def displayInformations(filename):
    reservedMemory = config.NUMBER_OF_CHANNELS * config.BYTES_PER_POINT * config.SAMPLES_PER_SECOND * \
                   config.SECONDS_PER_MINUTE * config.MAX_NUMBER_OF_MINUTES_OF_RECORDING / 1000000

    print("Current acquisition session's configuration:")
    print("    File: %s" % filename)
    print("    Maximum recording time: %s minutes" % config.MAX_NUMBER_OF_MINUTES_OF_RECORDING)
    print("    Number of electrodes: %s" % config.NUMBER_OF_CHANNELS)
    print("    Samples per second: %s" % config.SAMPLES_PER_SECOND)
    print("    Reserved Memory: %s MB" % reservedMemory)


def verifyRecordingsDirectory(recordingsDirectory, filename):
    # Verify existence
    if os.path.exists(recordingsDirectory): 
	if os.path.isdir(recordingsDirectory):
            try:
                fullPath = os.path.join(recordingsDirectory, filename)
                f = open(fullPath, 'w')
                f.close()
                os.remove(fullPath)
                return fullPath
            except Exception:
                print("Could not create file in chosen recordings directory.")
                sys.exit(0)
        else:
            print("Chosen recordings directory is not a directory.")
            sys.exit(0)
    else:
        # create it
        try:
            print("Creating directory '%s'" % recordingsDirectory)
            os.makedirs(recordingsDirectory)
        except:
            print("Could not make directory '%s'." % recordingsDirectory)
        try:
            fullPath = os.path.join(recordingsDirectory, filename)
            f = open(fullPath, 'w')
            f.close()
            os.remove(fullPath)
            return fullPath
        except Exception:
            print("Could not create file in chosen recordings directory.")
            sys.exit(0)


def main():
    """ Used to control all of the processes launched. Communication is assumed by the various control pipes and queues.
    """
    # Here is an array to hold on to all parent control pipes
    controlPipes = []
    
    # An array to hold all children control pipes
    childPipes = []
    
    # An array to hold all processes
    processes = []
    
    # Exit Code : Indicates if the application must exit
    exitCode = False
 
    # Verify we run the software as root
    if not verifyWeAreRoot():
        print("This script must be executed as root.  Run as sudo, or execute 'su -' and enter the root password.")
        sys.exit(2)

    filename = generateFilename()
    filename = verifyRecordingsDirectory(config.RECORDING_DIRECTORY, filename)

    # Display various informations related to the current acquisition session.
    displayInformations(filename)

    logger = logging.getLogger("Main")

    # Timing Queue : This is used to start acquiring events at the same time as EEG.
    timingQueue = Queue()

    # Data visualisations
    visualDataQueue = Queue()
    visualEventQueue = Queue()
    parentControlPipe, childControlPipe = Pipe()
    controlPipes.append(parentControlPipe)
    childPipes.append(childControlPipe)
    visualiser = Process(target=Grapher.execute, args=(visualDataQueue, visualEventQueue, childControlPipe))
    visualiser.start()
    
    # Event Writer
    eventQueue = Queue()
    parentControlPipe, childControlPipe = Pipe()
    controlPipes.append(parentControlPipe)
    childPipes.append(childControlPipe)
    eventWriter = Process(target=EventWriter.execute, args=(eventQueue, childControlPipe, filename + ".log",))
    eventWriter.start()

    # Ethernet Event Acquisition
    parentControlPipe, childControlPipe = Pipe()
    controlPipes.append(parentControlPipe)
    childPipes.append(childControlPipe)
    eventAcquisition = Process(target=EthernetEventAcquisition.execute, args=(eventQueue, childControlPipe, timingQueue, visualEventQueue))
    eventAcquisition.start()
    # For greater priority
    nice_value = -19
    try:
        os.system("renice -n %d %d > /dev/null" % (nice_value, eventAcquisition.pid))
    except Exception:
        logger.error("Could not execute renice command: running in NON-realtime mode.")

    # Analog Data Writer
    dataQueue = Queue()
    parentControlPipe, childControlPipe = Pipe()
    controlPipes.append(parentControlPipe)
    childPipes.append(childControlPipe)
    analogDataWriter = Process(target=AnalogDataWriter.execute, args=(dataQueue, childControlPipe, filename + ".mat",))
    analogDataWriter.start()

    # Fake Analog Data Generator
    parentControlPipe, childControlPipe = Pipe()
    controlPipes.append(parentControlPipe)
    childPipes.append(childControlPipe)
    analogDataGenerator = Process(target=FakeAnalogDataGenerator.execute, args=(dataQueue, childControlPipe, timingQueue, visualDataQueue))
    analogDataGenerator.start()

    # Data Acquisition
    #parentControlPipe, childControlPipe = Pipe()
    #controlPipes.append(parentControlPipe)
    #childPipes.append(childControlPipe)
    #analogDataGenerator = Process(target=AnalogDataAcquisition.execute, args=(dataQueue, childControlPipe, timingQueue, visualDataQueue))
    #analogDataGenerator.start()
    ## For greater priority
    #nice_value = -19
    #os.system("renice -n %d %d" % (nice_value, eventAcquisition.pid))

    # Controller
    parentControlPipe, childControlPipe = Pipe()
    controlPipes.append(parentControlPipe)
    childPipes.append(childControlPipe)
    controller = Controller.TerminalController()

    time.sleep(1)
    controller.init(childControlPipe)
    controller.start()

    # Control the usual signals
    while exitCode is False:
        # for each control pipe
        for controlPipe in controlPipes:
            # If there is something...
            if controlPipe.poll():
                command = controlPipe.recv()
                # If exit...
                if command == "exit":
                    logger.info("Sending exit signal...")
                    # Wait for all processes to exit, and wait for them to quit.
                    for controlPipe in controlPipes:
                        controlPipe.send("exit")
                    for process in processes:
                        process.join()
                    exitCode = True
                # If it is a start signal, tell all to start.
                elif command == "start":
                    logger.info("Sending start signal...")
                    for pipe in controlPipes:
                        pipe.send("start")
                # Else, we have a problem.
                else:
                    logger.info("Wrong command: '%s'" % command)

if __name__ == "__main__":
    main()
