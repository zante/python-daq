__author__ = 'lemelino'

import logging

import numpy
import PyDAQmx
from PyDAQmx import *

import commons
import config

# Initializes the logger
logger = logging.getLogger("DataAcquisition")

# Global exit code accessible from everywhere in the program.
exitCode = False

def createDataMatrix(controlPipe):
    """Creates the buffer used to save the acquisition data.  This buffer should be at least as large as the
    number of channels * the number of samples per second * the number of bytes of the data stocked."""
    dataMatrix = None
    try:
        # We create a data matrix sufficiently large to hold on to enough samples for a full second.
        dataMatrix = numpy.ndarray((config.NUMBER_OF_CHANNELS, config.SAMPLES_PER_SECOND), dtype=numpy.float64)
        logger.info("Successfully created data acquisition matrix.")
    except Exception:
        logger.critical("Could not allocate data acquisition matrix.")
        controlPipe.send(exit)
    return dataMatrix


class CallbackTask(PyDAQmx.Task):
    """This represents the task executed by the NiDAQmx driver to fetch 1000 samples per channel per second in
    the current data buffer."""

    def __init__(self, controlPipe, dataQueue, visualDataQueue):
        """Initializes the task by generating the data acquisition matrix and configuring the acquisition."""

        PyDAQmx.Task.__init__(self)
        
        if controlPipe is not None:
            self.controlPipe = controlPipe
        else:
            logger.critical("No control pipe available. Exiting.")
            exitCode = True

        if dataQueue is not None:
            self.dataQueue = dataQueue
        else:
            logger.critical("No dataQueue available. Exiting.")
            exitCode = True

        if visualDataQueue is not None:
            self.visualDataQueue = visualDataQueue
        else:
            logger.critical("No visualDataQueue available. Exiting.")
            exitCode = True

        # We create the data buffer.
        try:
            self.data = createDataMatrix(controlPipe)
        except Exception:
            logger.critical("Could not create the temporary acquisition matrix. Exiting.")
            exitCode = True

        # We generate the event acquisition channels string.  This is required, as we do not know how many channels
        # will be used beforehand.

        # declare array
        channel_parts = []

        # Declare the channels that will be used in the array
        for i in range(config.NUMBER_OF_CHANNELS):
            channel_parts.append("{}/{}".format(config.NI_DEVICE_TO_USE, "ai{}".format(i)))

        # Implode array into a single comma-separated string
        listOfChannels = ",".join(channel_parts)

        # We configure the Voltage acquisition channels. We merge many hardware channels in one software "channel".
        self.CreateAIVoltageChan(listOfChannels, "", DAQmx_Val_RSE, config.MIN_VOLTAGE, config.MAX_VOLTAGE,
                                 DAQmx_Val_Volts, None)

        # We configure the sampling clock
        self.CfgSampClkTiming("", config.SAMPLES_PER_SECOND, DAQmx_Val_Rising, DAQmx_Val_ContSamps,
                              config.SAMPLES_PER_SECOND)

        # We register both callback to events.
        self.AutoRegisterEveryNSamplesEvent(DAQmx_Val_Acquired_Into_Buffer, config.SAMPLES_PER_SECOND, 0)
        self.AutoRegisterDoneEvent(0)

    def EveryNCallback(self):
        """Every second, send the data acquired to dataQueue, which is processed by the Analog Data Writer module."""
        read = int32()
        self.ReadAnalogF64(config.SAMPLES_PER_SECOND, 1.1, DAQmx_Val_GroupByScanNumber,
                           self.data, config.SAMPLES_PER_SECOND, byref(read), None)
        try:
            # When we finish an acquisition, we push it to the dataQueue, where it will be processed by the data writer.
            self.dataQueue.put(self.data)
            self.visualDataQueue.put(self.data)
        except Exception:
            logger.critical("Could not push current data for queueing. Queue probably full.")
            self.controlPipe.send(exit)
            exitCode = True

        logger.debug("Successfully pushed data.")

    def DoneCallback(self, status):
        """When the acquisition is called, this function is called."""
        logger.debug("Status", status.value)
        return 0  # The function should return an integer


def onExit():
    """This function is called when the processing is stopped."""
    logger.info("Closing Analog Data Generator...")
    logger.info("Analog Data Generator closed.")


def execute(dataQueue, controlPipe, timingPipe, visualDataAcquisition):
    """Main processing function."""
    global exitCode

    logger.info("Starting Analog Data Generator Thread.")

    # Create PyDAQ acquisition task.
    task = CallbackTask(controlPipe, dataQueue, visualDataAcquisition)

    # Spin while the program does not receive the start signal.
    commons.spinUntilStart(controlPipe, logger)

    # Create the task and its callbacks
    task.StartTask()

    # We log the beginning time to synchronize with the event acquisition program.
    # The timing is sent in a pipe to the event acquisition software.
    timing = time.time()
    timingPipe.put(timing)
    logger.info("Starting Data acquisition at %s" % timing)

    # Loop until we receive the signal to stop
    while exitCode is False:
        if controlPipe.poll():
            data = controlPipe.recv()
            if data == "exit":
                exitCode = True

    # Stopping the recording
    task.StopTask()
    task.ClearTask()

    onExit()
