__author__ = 'lemelino'

###### CONFIGURATION #######

# Recordings directory.  Recording sessions will be sent to this directory.
RECORDING_DIRECTORY = "/home/acquisition/Desktop/recordings/"

# This is used to represent the size of the data type used to store the data acquisition results.
# Currently, this is a float64, which is 8 bytes wide.
BYTES_PER_POINT = 8

# Measures the maximal/minimal values that should be measured.
MIN_VOLTAGE = -5.0
MAX_VOLTAGE = 5.0

# Number of channels / electrodes to use.  Make sure that they are are sequentially plugged into the
# amplifier beforehand, starting from '0'.
NUMBER_OF_CHANNELS = 32

# Sampling rate used, in Hertz.
SAMPLES_PER_SECOND = 500

# If more than one National Instruments devices are plugged in, you might need to specify 'Dev2' or 'Dev3'
NI_DEVICE_TO_USE = "Dev1"

# Since the size of data acquired can quickly become particularly large, a cap has been set to allocate all memory
# beforehand.  Change this to a lower value if the recording takes too much space, or to a larger value if the
# experiments are longer than two hours.
MAX_NUMBER_OF_MINUTES_OF_RECORDING = 240

# Here is the list of textual descriptions representing each event code.  Each event is assigned a number, which is
# represented in the following array according to its position.  Thus, event '0' is the first event of the list, event
# '1' if the second, etc.
EVENTS = ["target", "response", "T01", "T02", "T03"]

# Ethernet port to use (if ethernet is preferred)
ETHERNET_PORT = 779

# Serial port interface on which we expect to receive the event codes.
#SERIAL_INTERFACE = "COM7"

# Indicates the Baud rate (pulses per second) which corresponds to the usual bit rate of the serial port interface.
# BAUD_RATE = 9600
############################

###### CONSTANTS ######
SECONDS_PER_MINUTE = 60
BYTES_PER_MEGABYTE = 1000000
#######################
