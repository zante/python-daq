#!/bin/python

import pygame
import pygame.font
import pygame.locals

import config
import numpy as np
import time
import signal
import math
import sys

### Basic layout and graphics ###
WINDOW_WIDTH = 800
WINDOW_HEIGHT = 600
LEFT_RECTANGLE_WIDTH = 20
BOTTOM_RECTANGLE_HEIGHT = 40
FONT_SIZE = 14

##### Data-related constants #####

# Number of points per electrode per sample
POINTS_PER_EEG_SECTION = config.SAMPLES_PER_SECOND


# Number of pixels used to represent a section
PIXELS_PER_SECTION = 60

# Maximum number of EEG sections.  Will probably not be reached, as
# the system also deallocates EEG sections when they disappear from the
# screen
MAX_EEG_SECTIONS = 200

# Number of sections received per second
SECTIONS_PER_SECOND = 1

# 'Maximal' normal voltage
FULL_VOLTAGE = config.MAX_VOLTAGE

# Number of electrodes
NB_ELECTRODES = 32

#Name of each electrode
ELECTRODES_LABELS = ["E0","E1","E2","E3","E4","E5","E6","E7","E8",
                     "E9","E10","E11","E12","E13","E14","E15","E16","E17",
                     "E18","E19","E20","E21","E22","E23","E24","E25","E26",
                     "E27","E28","E29","E30", "E31"]

# Time to sleep on each loop
SLEEP_TIME_ON_LOOP = 0.2

POINTS_PER_PIXEL = POINTS_PER_EEG_SECTION / PIXELS_PER_SECTION

global running
running = True

# TODO:
# Clock start on 'start' event
# Insert events at the correct position, knowing how much pixels is one second and how much time has elapsed.
# 


##########################################################
# BUFFER DATA ORGANISATION:
##########################################################
#
# The data buffer is organised in a such as to facilitate the acquisition
# and storage of discrete portions of data received from a live acquisition
# from the main program.
# Thus, we have sections, which represent data for several electrodes acquired
# directly from an ongoing data acquisition.  These sections are appended at
# the end of the buffer, and discarded when they can no longer be shown on the
# screen.
# Electrodes, then, contain a number of data points for a single electrode, which are
# stored for a single section.  They are numbered from 0 to X, where X is the number
# of electrodes used.
# Finally, data points are stored as an height on the screen: They represent the
# height at which they should be drawn on the screen.  Thus, it is a position.
#
# TLDR;
# dataBuffer
#   section
#     electrode
#       position



class Grapher(object):
    def __init__(self, eegQueue, eventQueue, controlPipe):
 
        # Variables used for tests.
        self.test = 0
        self.wazza = 0

        if len(ELECTRODES_LABELS) != NB_ELECTRODES:
            print "%s electrodes labels assigned, but we have %d electrodes!" % (len(ELECTRODES_NAMES), NB_ELECTRODES)
            sys.exit(-2)
       
        # We prepare our data queues
        self.eegQueue = eegQueue
        self.eventQueue = eventQueue
        self.controlPipe = controlPipe

        # This variable is used to see if we need to update
        self.windowUpdateRequired = True
        
        # This variable counts the number of sections acquired.
        # We can measure time elapsed with this.
        self.dataSectionsCounter = 0
        self.dataSectionsErased = 0

        pygame.init()

        # Create array of events
        self.displayedEvents = []
        # Create array of EEG buffers
        self.displayedEEG = []
        
        # Create window
        self.window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
        pygame.display.set_caption("Visualisation")

        # Create electrodes rectangles
        self.buildLayout()

        #We prepare our buffers
        self.eegBuffer = []
        self.eventBuffer = []

        #create colors
        self.blackColor = pygame.Color(0,0,0)
        self.whiteColor = pygame.Color(255,255,255)
        self.grayColor = pygame.Color(80,80,80)
        self.redColor = pygame.Color(255, 0, 0)
        
        self.font = pygame.font.Font(None, FONT_SIZE)


    # ==== Drawing functions ====================

    def buildLayout(self):
        '''Builds the layout used by the application.'''
        windowHeight = self.window.get_height()
        windowWidth = self.window.get_width()

        self.layout = dict()
 
        # This is used to represent the rectangle where we present the electrode numbers
        self.layout['leftRectangle'] = pygame.Rect(0, 0, LEFT_RECTANGLE_WIDTH, 
                    windowHeight)

        # This is used to present our EEG Data
        self.layout['eegRectangle'] = pygame.Rect(LEFT_RECTANGLE_WIDTH, 
                    0, 
                    windowWidth - LEFT_RECTANGLE_WIDTH, 
                    windowHeight - BOTTOM_RECTANGLE_HEIGHT)

        # This is used to present the events timeline
        self.layout['bottomRectangle'] = pygame.Rect(LEFT_RECTANGLE_WIDTH, 
                    windowHeight - BOTTOM_RECTANGLE_HEIGHT, 
                    windowWidth, 
                    BOTTOM_RECTANGLE_HEIGHT)

        # This is used to store the difference in height between each electrode
	self.layout['electrodeSpaceHeight'] = self.layout['eegRectangle'].h / NB_ELECTRODES


    def drawZeroVoltLines(self):
        '''Used to draw the zero volt line for each electrode.'''
        for i in range(NB_ELECTRODES):
            # We draw the Zero-level line of the electrode in the middle of its section.
            zero = (i + 0.5) * self.layout['electrodeSpaceHeight']
            # Draw it!
            pygame.draw.line(self.window, 
                self.grayColor, 
                (self.layout['eegRectangle'].x, zero), 
                (self.layout['eegRectangle'].x + self.layout['eegRectangle'].w, zero), 
                1)


    def drawElectrodeNo(self):
        '''Used to draw the electrode's name next to its zero volt line.'''
        # We position the electrode name in the middle of its space
        xPosition = LEFT_RECTANGLE_WIDTH / 2

        # For each electrode, we render the label corresponding to its name. 
        for i in range(NB_ELECTRODES):
            yPosition = (i + 0.5) * self.layout['electrodeSpaceHeight']
            text = self.font.render(ELECTRODES_LABELS[i], False, self.grayColor)
            textpos = text.get_rect(center=(xPosition, yPosition))
            self.window.blit(text, textpos)


    def drawSections(self):
        '''Used to draw the lines separating layout elements'''
        pygame.draw.rect(self.window, self.grayColor, self.layout['leftRectangle'], 1)
        pygame.draw.rect(self.window, self.grayColor, self.layout['eegRectangle'], 1)
        pygame.draw.rect(self.window, self.grayColor, self.layout['bottomRectangle'], 1)


    def drawInterface(self):
        '''Used to draw the visual interface.  This does not includes the EEG lines.'''
        # CLear the window
        self.window.fill(self.blackColor)
        # Draw the line separating each section
        self.drawSections()
        # Draws the label of the electrodes
        self.drawElectrodeNo()
        # Draw the zero volt line
        self.drawZeroVoltLines()
 

    def drawLinesOnEEG(self):
        '''We draw the data with their height on the screen.'''
        leftBaseLine = self.layout['eegRectangle'].x
        maxNbSectionsThatFit = self.layout['eegRectangle'].w / PIXELS_PER_SECTION
        
        #Drawing from most recent data to oldest
        x = leftBaseLine
        for section in self.eegBuffer[-maxNbSectionsThatFit:]:
            self.drawSectionOnEEG(section, x)
            x += PIXELS_PER_SECTION
            pygame.draw.line(self.window, self.grayColor, (x, 1), (x, self.layout['eegRectangle'].h - 1), 1)
        
        for section in self.eegBuffer[-len(self.eegBuffer):-maxNbSectionsThatFit + 1]:
            self.eegBuffer.remove(section)
            self.dataSectionsErased += 1

        
    def drawSectionOnEEG(self, section, baseLine):
        '''We draw a single of an acquisition.'''
        for electrode in section:
            self.drawElectrodeOnEEG(electrode, baseLine)


    def drawElectrodeOnEEG(self, electrode, xPosition):
        '''We draw each point captured on an electrode.'''
        for point in electrode:
            pygame.draw.rect(self.window, self.whiteColor, pygame.Rect((xPosition, point), (2, 2)))
            xPosition = xPosition + 1
  
 
    def drawTimeline(self):
        # We process events from the most recent to the oldest
        for event in self.eventBuffer[-len(self.eventBuffer):]:
            eventTime = event[0]
            currentTiming = self.startTime
            # xPositionOnScreen = self.layout['eegRectangle'].x + \
            #         ((float(eventTime) / SECTIONS_PER_SECOND) * PIXELS_PER_SECTION) - (self.dataSectionsErased * PIXELS_PER_SECTION)
            # TODO
            print "event: %d" % float(eventTime)
            print "Timing: %d" % currentTiming
            print "datasectionserased: %d" % self.dataSectionsErased
            xPositionOnScreen = self.layout['eegRectangle'].x + \
                     ((float(eventTime) / SECTIONS_PER_SECOND) * PIXELS_PER_SECTION) - (self.dataSectionsErased * PIXELS_PER_SECTION)
            print "XPOSITION: %d" % xPositionOnScreen 
            
            if(xPositionOnScreen <= self.layout['eegRectangle'].x):
                self.eventBuffer.remove(event)
            else:
                pygame.draw.line(self.window, self.redColor, (xPositionOnScreen, 1), (xPositionOnScreen, self.layout['eegRectangle'].h - 1), 2)
                # Print textual info
                eventText = event[1]
                verticalSpace = self.font.size(eventText)[0]

                yPosition = self.layout['bottomRectangle'].y + verticalSpace / 2
                text = self.font.render(str(eventText), False, self.grayColor)
                textpos = text.get_rect(center=(xPositionOnScreen, yPosition))
                self.window.blit(text, textpos)

                yPosition = self.layout['bottomRectangle'].y + verticalSpace / 2 + verticalSpace
                text = self.font.render(str(eventTime), False, self.grayColor)
                textpos = text.get_rect(center=(xPositionOnScreen, yPosition))
                self.window.blit(text, textpos)
                


    def drawEEG(self):
        '''Draws the EEG window.'''
        self.drawLinesOnEEG()
        # Draws the timeline
        self.drawTimeline()


    def drawScreen(self):
        '''Draws the screen, then updates it.'''
        self.drawInterface()
        self.drawEEG()
        pygame.display.update()
        self.windowUpdateRequired = False


    # ==== Data functions ====================

    def isEEGDataAvailable(self):
        '''returns true if there is incoming EEG data on the pipe.'''
        # todo remove
        if self.eegQueue is None:
            return True
        else:
            return not self.eegQueue.empty()


    def isEventDataAvailable(self):
        '''Returns true if there is incoming event data on the pipe.'''
        if self.eventQueue is None:
            return True
        else:
            return not self.eventQueue.empty()


    def grabEEGData(self):
        '''Gets EEG Data from the pipe.'''
        self.windowUpdateRequired = True
        
        # Grab data from pipe
        if self.eegQueue is None:
            value = (math.floor(self.test / 5) % 2) * 5
            section = np.ndarray((NB_ELECTRODES, POINTS_PER_EEG_SECTION), dtype=np.int)
            for noElectrode in range(NB_ELECTRODES):
                for noPoint in range(POINTS_PER_EEG_SECTION):
                    section[noElectrode][noPoint] = value
            self.test = self.test + 1
            return section
        else:
            return self.eegQueue.get()


    def grabEventData(self):
        '''Gets event data from the pipe.'''
        if self.eventQueue is None:
            self.windowUpdateRequired = True
            self.wazza += 0.25
            return [self.wazza, "Test", "0"]
        else:
            return self.eventQueue.get()


    def averageEEG(self, eegSection):
        'Averages a certain number of data points in order to compress the amount of data shown at a time.'
        #creates a new float array to store a section of data.
        newSection = np.ndarray((NB_ELECTRODES, PIXELS_PER_SECTION), dtype=np.float)
        # For data in each electrode
        for noElectrode in range(NB_ELECTRODES):
            # For each pixel that should be covered with data from the section
            for noPixel in range(PIXELS_PER_SECTION):
                accumulator = 0.0
                # For each pixel, we obtain a range of data that should be "condensed"
                # and averaged in it.
                for point in range(noPixel * POINTS_PER_PIXEL, noPixel * POINTS_PER_PIXEL + POINTS_PER_PIXEL):
                    accumulator += eegSection[noElectrode][point]
                    
                accumulator = accumulator / POINTS_PER_PIXEL
                
                # We assign the new value to the pixel.
                newSection[noElectrode][noPixel] = accumulator

        return newSection


    def averagedToHeightDiff(self, averagedSection):
        'Here, we convert the averaged data to a proper height on the screen.'
        # We create a new array to stock
        heightSection = np.ndarray((NB_ELECTRODES, PIXELS_PER_SECTION), dtype=np.int)

        for electrodeNo in range(NB_ELECTRODES):
            for pointNo in range(len(averagedSection[electrodeNo])):
                # We obtain the zero voltage line's height on the screen
                zeroLineHeight = (electrodeNo + 0.5) * self.layout['electrodeSpaceHeight']
                #Bam, we get the full height on the screen
                heightSection[electrodeNo][pointNo] = -((averagedSection[electrodeNo][pointNo] / FULL_VOLTAGE) * self.layout['electrodeSpaceHeight']) + zeroLineHeight
        return heightSection


    def addEEGToBuffer(self, newData):
        '''This function is used to append a new section to the buffer.'''
        # if buffer is full, remove oldest element
        while len(self.eegBuffer) > MAX_EEG_SECTIONS:
            del self.eegBuffer[0]

        # We count one more section acquired
        self.dataSectionsCounter = self.dataSectionsCounter + 1
        # We average the data for each pixel
        averaged = self.averageEEG(newData)
        # We obtain the height of each value
        height = self.averagedToHeightDiff(averaged)
        # Append the new section
        self.eegBuffer.append(height)


    def addEventToBuffer(self, newEvent):
        '''Adds an event to the buffer.'''
        self.eventBuffer.append(newEvent)


    # ==== Main Logic ====================

    def onExit(self):
        '''On Quitting, execute the following functions'''
        pygame.quit()

        if self.controlPipe:
            self.controlPipe.send("exit")


    def waitForStartSignal(self):
        global running
        if self.controlPipe is not None:
            while(True):
                if self.controlPipe.poll():
                    data = self.controlPipe.recv()
                    if data == "start":
                        self.startTime = time.time()
                        print "Starting timer at time %s" % self.startTime
                        return
                    elif data == "exit":
                        running = False
                        return
        else:
            self.startTime = time.time()


    def execute(self):
        '''Main loop'''
        global running

        self.waitForStartSignal()

        while running:
            #todo replace next two ifs with while
            #TODO
            if self.isEEGDataAvailable():
            #while self.isEEGDataAvailable():
                data = self.grabEEGData()
                self.addEEGToBuffer(data)
            if self.isEventDataAvailable():
            #while self.isEventDataAvailable():
                event = self.grabEventData()
                self.addEventToBuffer(event)
            self.drawScreen()
                
            # Handle events
            for event in pygame.event.get():
                if event.type == pygame.locals.QUIT:
                    print "Exiting..."
                    running = False

            #Process control pipe
            if self.controlPipe and self.controlPipe.poll():
                data = self.controlPipe.recv()
                if data == "exit":
                    running = False

            # Sleep for a short while
            time.sleep(SLEEP_TIME_ON_LOOP)
        self.onExit()

def execute(eeg, event, pipe):
    grapher = Grapher(eeg, event, pipe)
    grapher.execute()
    

if __name__ == "__main__":
    execute(None, None, None)
