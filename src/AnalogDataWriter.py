__author__ = 'lemelino'

import os.path
import logging
import time

import scipy
import scipy.io
import numpy

import config
import commons

logger = logging.getLogger("AnalogWriter")
exitCode = False

def verifyFileSystem(controlPipe, fileName):
    """Verifies if enough space exists on disk to receive the largest possible file and that no file with the same
    name already exists.  We exit if the system is not OK."""

    # Make sure the file specified by the pathdoes not exist.
    if os.path.isfile(fileName):
        logger.error("File %s already exists! Exiting." % fileName)
        controlPipe.send("exit")
        exitCode = True

    # Make sure the remaining space on disk is equal or greater than the maximum size possible. 
    sizeRequired = config.NUMBER_OF_CHANNELS * config.BYTES_PER_POINT * \
                   config.SAMPLES_PER_SECOND * config.SECONDS_PER_MINUTE * \
                   config.MAX_NUMBER_OF_MINUTES_OF_RECORDING
    remainingDiskSpace = commons.get_free_space_mb(".") * 1000000
    if remainingDiskSpace <= sizeRequired:
        logger.critical("WARNING! Disk space will not be sufficient to store the new MATLAB matrix.\n"
                        "Available: %s MBytes. Remaining: %s MBytes. Exiting." % (sizeRequired / 1000000,
                                                                                  remainingDiskSpace / 1000000))
        controlPipe.send("exit")
        exitCode = True


def prepareMemory(controlPipe):
    """We create a buffer large enough to receive the full length of the recording.
    Exits if not enough space remaining."""

    #First dimension is number of channels. Second is time.
    matrix = None
    # Try to allocate space n memory for data acquisition.  If impossible, cancel the execution of the program
    try:
        matrix = numpy.ndarray((config.NUMBER_OF_CHANNELS, config.SAMPLES_PER_SECOND * config.SECONDS_PER_MINUTE *
                                                           config.MAX_NUMBER_OF_MINUTES_OF_RECORDING),
                               dtype=numpy.float)
    except Exception:
        sizeInMB = config.NUMBER_OF_CHANNELS * config.BYTES_PER_POINT * config.SAMPLES_PER_SECOND * \
                   config.SECONDS_PER_MINUTE * config.MAX_NUMBER_OF_MINUTES_OF_RECORDING / 1000000
        logger.critical("Could not allocate memory for Analog acquisition matrix: {} MB is too much." % sizeInMB)
        controlPipe.send("exit")
        exitCode = True

    logger.debug("Successfully allocated %s bytes of memory to the matrix." % matrix.nbytes)
    return matrix


def writeDataInMemory(matrix, data, baseIndex, controlPipe):
    """Takes the received data, usually a (nb of channels * samples per second) matrix, and writes it in the next free
     space in the larger matrix."""

    # Write data in memory in next available space in 
    logger.debug("Beginning to write to memory...")
    t0 = time.time()

    # The baseIndex represents the y index where we should currently be writing.
    try:
        for x in range(config.NUMBER_OF_CHANNELS):
            for y in range(config.SAMPLES_PER_SECOND):
                matrix[x, y + baseIndex] = data[x, y]
    except Exception:
        logger.error("Reached the max size possible for the acquisition matrix in memory.  Stopping.")
        controlPipe.send("exit")
        exitCode = True

    logger.debug("Finished writing to memory. Took %s seconds." % (time.time() - t0))

    # We return the new index corresponding the index next to last we wrote.
    return baseIndex + config.SAMPLES_PER_SECOND


def writeDataToFile(fileName, matrix, finalIndex, controlPipe):
    """Writes the great matrix to a .MAT Matlab matrix file. The name of the variable for EEGLab importation is
    'data'."""
    try:
        # We shrink the total matrix to a smaller size by eliminating the space that was not recorded.
        logger.info("Shrinking matrix...")
        currentSizeMB = matrix.nbytes / config.BYTES_PER_MEGABYTE
        matrix = matrix[:config.NUMBER_OF_CHANNELS, :finalIndex]
        finalSizeMB = matrix.nbytes / config.BYTES_PER_MEGABYTE
        logger.info("Finished shrinking the matrix. Went from %s MB to %s MB." % (currentSizeMB, finalSizeMB))

        # We write the matrix to a MATLAB file.
        logger.info("Saving matrix to MATLAB format in file '%s'..." % fileName)
        scipy.io.savemat(fileName, mdict={'data': matrix})
        logger.info("Finished saving data to file.")
    except Exception as e:
        logger.critical("Could not write data to MATLAB file.")


def onExit(fileName, matrix, finalIndex, controlPipe):
    """This function is run when we stop the recording."""
    logger.info("Closing Event Analog Data Writer...")
    writeDataToFile(fileName, matrix, finalIndex, controlPipe)
    logger.info("Event Writer closed.")


def execute(eventQueue, controlPipe, fileName):
    """Execute the main program."""

    global exitCode

    logger.info("Starting Analog Data Writer Thread.")

    if controlPipe is None:
        logger.critical("Control Pipe is absent. Stopping.")
        controlPipe.send("exit")

    if eventQueue is None:
        logger.critical("Event Queue is absent. Stopping.")
        eventQueue.send("exit")

    # We prepare the system
    verifyFileSystem(controlPipe, fileName)
    matrix = prepareMemory(controlPipe)
    baseIndex = 0

    # Busy waiting.
    commons.spinUntilStart(controlPipe, logger)

    while exitCode is False:
        # process the contents of the data queue.
        while not eventQueue.empty():
            data = eventQueue.get()
            baseIndex = writeDataInMemory(matrix, data, baseIndex, controlPipe)

        #Process control pipe
        if controlPipe.poll():
            data = controlPipe.recv()
            if data == "exit":
                exitCode = True

    onExit(fileName, matrix, baseIndex, controlPipe)
