__author__ = 'lemelino'

import ctypes
import os
import platform
import logging

# Definitions
FORMAT = '%(name)-25s %(asctime)-25s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

# Functions

def get_free_space_mb(folder):
    """ Return folder/drive free space (in bytes) """
    if platform.system() == 'Windows':
        free_bytes = ctypes.c_ulonglong(0)
        ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(folder), None, None, ctypes.pointer(free_bytes))
        return free_bytes.value / 1024 / 1024
    else:
        st = os.statvfs(folder)
        return st.f_bavail * st.f_frsize / 1024 / 1024


def spinUntilStart(controlPipe, logger):
    """Busy wait routine.  When the signal is received, we stop the waiting."""
    exitCode = False
    while not exitCode:
        if controlPipe.poll():
            data = controlPipe.recv()
            if data == "start":
                exitCode = True
                logger.info("Starting!")
            elif data == "stop" or data== "exit":
                logger.info("Aborting!")
                exit(0)
            else:
                print("Could not understand the following command: '%s'"% data)
